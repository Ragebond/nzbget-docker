FROM alpine:3.7
LABEL maintainer="ragebond"

ENV HUID 1200
ENV HGID 1200

RUN apk --no-cache add shadow libxml2 ncurses openssl wget; \
    addgroup -g 1200 -S appgroup; \
    adduser -u 1200 -D -S -s /sbin/nologin -G appgroup appuser
RUN wget https://nzbget.net/download/nzbget-latest-bin-linux.run && \
    sh nzbget-latest-bin-linux.run && \
    rm nzbget-latest-bin-linux.run

VOLUME /config
VOLUME /downloads

EXPOSE 6789

#USER appuser

CMD groupmod -o -g $HGID appgroup; \
    usermod -o -u $HUID appuser; \
    id appuser; \
    su appuser --s /bin/sh -c '\
    if [ ! -f "/config/nzbget.conf" ]; then \
             cp /nzbget/nzbget.conf /config/nzbget.conf; \
             sed -i -e "s~\(MainDir=\).*~\1/downloads~g" /config/nzbget.conf; \
    fi; \
    for i in completed intermediate nzb queue tmp scripts movies series music software; do \
      if [ ! -d "/downloads/$i" ]; then \
        mkdir -p /downloads/$i; \
      fi; \
    done; \
    /nzbget/nzbget -s -o outputmode=log -c /config/nzbget.conf; \
    '
