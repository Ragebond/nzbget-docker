A simple dockerfile to use NZBGet

Usage:

Create the config and downloads directories

Build the image:
```
docker build -t nzbget .
```

Run the container:
```
docker run -d \
  -p 6789:6789 \
  -v <full path to host config directory>:/config \
  -v <full path to host downloads directory>:/downloads \
  -e HUID=$(id -u <user who owns the config and downloads directories>) \
  -e HGID=$(id -g <user who owns the config and downloads directories>) \
  --name nzbget \
  nzbget
```
